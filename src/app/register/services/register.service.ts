import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { StudyProgram } from '../components/new/new.component';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  private _studyPrograms: StudyProgram[];
  constructor(private readonly _context: AngularDataContext) {}

  getAvailableStudyPrograms(): Promise<StudyProgram[]> {
    if (this._studyPrograms) {
      return Promise.resolve(this._studyPrograms);
    }
    return this._context
      .model('StudyPrograms/availableForEnrollment')
      .asQueryable()
      .take(1)
      .getItems()
      .then((studyPrograms) => {
        this._studyPrograms = studyPrograms;
        return Promise.resolve(studyPrograms);
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  }

  getStudyProgram(id: number | string): Promise<StudyProgram> {
    if (id == null) {
      return Promise.resolve(null);
    }
    const findProgram: StudyProgram = this._studyPrograms.find(
      (program: StudyProgram) => program.id === id
    );
    if (findProgram) {
      return Promise.resolve(findProgram);
    }
    return this._context
      .model('StudyPrograms')
      .where('isActive')
      .equal(true)
      .and('id')
      .equal(id)
      .expand('specialties($orderby=specialty asc), info')
      .getItem()
      .then((studyProgram) => {
        this._studyPrograms.push(studyProgram);
        return Promise.resolve(studyProgram);
      });
  }

  addStudyProgram(studyProgram: StudyProgram): void {
    const findIndex = this._studyPrograms.findIndex(
      (program) => program.id === studyProgram.id
    );
    if (findIndex === -1) {
      this._studyPrograms.push(studyProgram);
    }
  }
}
